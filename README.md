# README #

This README is a set up guide for the BankAccount application.

## Quick summary ##

Bank Account Application is a simple micro web service developed to mimic a “Bank Account” operations. It has been develloped using Java, Spring MVC, SQLite and Apache Tomcat Server.
## Version: 
1.0
## Environment ##
This application has been developed under the following environment:

1. Ubuntu 16.04
2. Java (JDK 1.8)
3. Apache Tomcat 7
4. Apache Maven 3.3.9
5. SQLite 3
6. Git 2.7.4

## Set up ##
We assume that Java, Maven and Tomcat are already well installed and configured.

* Check out the code from the repository by running the following command: 

```
#!script

git clone https://sasipa90@bitbucket.org/sasipa90/bankaccount.git
```

* Move to the project folder (**BankAccount**), edit the ***pom.xml*** file to point to the local instance of tomcat. Update the **url** to the tomcat instance, **username** and **password** information of the tomcat user with ***manager-script*** role. For more information about tomcat Users/Realm configuration click on the following [link](https://tomcat.apache.org/tomcat-7.0-doc/realm-howto.html#Overview).

```
#!xml

<plugins>
   <plugin>
      <groupId>org.apache.tomcat.maven</groupId>
      <artifactId>tomcat7-maven-plugin</artifactId>
      <configuration>
         <server>TomcatServer</server>
         <path>/BankAccount</path>
         <url>http://localhost:8080/manager/text</url>
         <username>admin</username>
          <password>admin</password>
      </configuration>
   </plugin>
</plugins>
```

* From the command line run the following command to build the project 
```
#!script

mvn package
```

* Deploy the application to your local tomcat instance by running the following script from the command line


```
#!script

mvn tomcat7:deploy
```
 if it is the first deployment or 
```
#!script

mvn tomcat7:redeploy
```
 to redeploy the application
  

You should be able to see a successful message from the screen that looks like  

![deployment success.png](https://bitbucket.org/repo/Rd6grz/images/1115283193-deployment%20success.png)


## API Services and parameters ##

**1. Balance**

URL: http://{server_url}/{application}/balance/{account number}
Method: GET

sample: http://localhost:8080/BankAccount/balance/ACC001

**ACC001** is set as default account

**2. Deposit**

URL: http://{server_url}/{application}/deposit

Method : POST

Parameters: 

* amount (required)
* accountNo (not required, set *ACC001* as default)

Sample Request: http://localhost:8080/BankAccount/deposit

Sample Response: "success":true,"statusCode":200,"message":"Successfully deposited $10000.0 to account No ACC001. Your new balance is $54400.0."}

**3. Withdraw** 

URL: http://{server_url}/{application}/withdraw

Method : POST

Parameters: 

* amount (required)
* accountNo (not required, set *ACC001* as default)

Sample Request: http://localhost:8080/BankAccount/withdraw

Sample Response: {"success":false,"statusCode":200,"message":"Exceeded Maximum Withdrawal Frequency"}

## Dependencies ##
1. Spring Web MVC
2. Spring JDBC
3. Log4j
4. Jackson JSON
5. SQLite JDBC
6. Commons Database Connection Pools


## Configuration ##

The applications has two main configurations:

1. bankaccount.properties: Has application rules and messages configurations.
2. log4j.properties : Has logging related configurations. Make sure that the configured logging directory exists. The default path is **/var/log/bank_account**

The configurations files can be updated but require the server to be restarted to take effect.

They can be found in tomcat ***/webapps/BankAccount/WEB-INF/classes/*** directory.

## Run and Generating Test Results ##

Run the test with the command 
```
#!shell

mvn surefire:test
```
 from the project root folder.

After the tests, the results will be shown as in the image below 

![test results.png](https://bitbucket.org/repo/Rd6grz/images/3658176811-test%20results.png)

## Code Coverage Report ##

To generate the code coverage report, run from the project root folder  
```
#!shell

mvn cobertura:cobertura
```
 or run 
```
#!script

mvn site 
```
to generate more the application site.

You can then access it through a browser in the target folder.

Sample path: file:///home/xxx/workspace/BankAccount/target/site/cobertura/index.html

![Code Coverage.png](https://bitbucket.org/repo/Rd6grz/images/628949423-Code%20Coverage.png)

## Author ##

* Parfait Pascal Sibomana <sasipa90@hotmail.com>