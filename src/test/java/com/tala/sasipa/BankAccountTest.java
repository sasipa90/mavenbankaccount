package com.tala.sasipa;

import static org.hamcrest.Matchers.hasKey;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Random;
import java.util.UUID;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.tala.sasipa.controller.BankAccountController;
import com.tala.sasipa.dao.BankAccountDAO;
import com.tala.sasipa.dao.impl.BankAccountDAOImpl;
import com.tala.sasipa.model.BankAccount;
import com.tala.sasipa.service.impl.BankAccountServiceImpl;
import com.tala.sasipa.utils.JSONResponse;
import com.tala.sasipa.utils.Props;
import com.tala.sasipa.utils.Utilities;
import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:springrest-servlet.xml"})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BankAccountTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static final String accountNo = UUID.randomUUID().toString();

    private final static Random r = new Random();

    private final static double rangeMin = 100;

    private final static double rangeMax = 1000;

    private final double depositAmount;

    private final double withrdrawAmount;

    public BankAccountTest() {

        depositAmount = rangeMax + (rangeMax - rangeMin) * r.nextDouble();

        withrdrawAmount = rangeMin + (rangeMax - rangeMin) * r.nextDouble();

        System.out.println(depositAmount + " - " + withrdrawAmount);

    }

    @Before
    public void setUp() {

        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        Props prop = (Props) webApplicationContext.getBean("props");
        bankAccountDAOImpl.setupAccount(accountNo, prop);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void balance() throws Exception {

        mockMvc.perform(get("/balance/" + accountNo)).andExpect(status().isOk())
                .andExpect(
                        content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasKey("success")))
                .andExpect(jsonPath("$", hasKey("statusCode")))
                .andExpect(jsonPath("$", hasKey("message")));

    }

    @Test
    public void deposit() throws Exception {

        mockMvc.perform(post("/deposit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("amount", String.valueOf(depositAmount))
                .param("accountNo", accountNo))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasKey("success")))
                .andExpect(jsonPath("$", hasKey("statusCode")))
                .andExpect(jsonPath("$", hasKey("message")));

    }

    @Test
    public void withdraw() throws Exception {

        mockMvc.perform(post("/withdraw")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("amount", String.valueOf(withrdrawAmount))
                .param("accountNo", accountNo))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasKey("success")))
                .andExpect(jsonPath("$", hasKey("statusCode")))
                .andExpect(jsonPath("$", hasKey("message")));

    }

    @Test
    public void testGetter() throws Exception {

        Props p = (Props) webApplicationContext.getBean("props");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) webApplicationContext
                .getBean("jdbcTemplate");
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        BankAccountServiceImpl bankAccountService = (BankAccountServiceImpl) webApplicationContext
                .getBean("bankAccountService");
        BankAccountController bankAccountController = (BankAccountController) webApplicationContext
                .getBean("bankAccountController");

        assertEquals(bankAccountController.getProps(), p);
        assertEquals(bankAccountController.getBankAccountService(),
                bankAccountService);
        assertEquals(bankAccountDAOImpl.getJdbcTemplate(), jdbcTemplate);
        assertEquals(bankAccountService.getBankAccountDAO(),
                bankAccountDAOImpl);

    }

    @Test
    public void testSetter() throws Exception {
        Props prop = (Props) webApplicationContext.getBean("props");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) webApplicationContext
                .getBean("jdbcTemplate");
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        BankAccountServiceImpl bankAccountService = (BankAccountServiceImpl) webApplicationContext
                .getBean("bankAccountService");
        BankAccountController bankAccountController = (BankAccountController) webApplicationContext
                .getBean("bankAccountController");

        bankAccountController.setProps(prop);
        assertEquals(bankAccountController.getProps(), prop);

        bankAccountController.setBankAccountService(bankAccountService);
        assertEquals(bankAccountController.getBankAccountService(),
                bankAccountService);

        bankAccountDAOImpl.setJdbcTemplate(jdbcTemplate);
        assertEquals(bankAccountDAOImpl.getJdbcTemplate(), jdbcTemplate);

        bankAccountService.setBankAccountDAO(bankAccountDAOImpl);
        assertEquals(bankAccountService.getBankAccountDAO(),
                bankAccountDAOImpl);

    }

    @Test
    public void testSQLException() throws Exception {

        BankAccountDAOImpl bankAccountDAOMock = Mockito.mock(BankAccountDAOImpl.class);
        Props propsMock = Mockito.mock(Props.class);
        bankAccountDAOMock.setJdbcTemplate(null);
        BankAccount bAcc = bankAccountDAOMock.getAccountInformations(accountNo, propsMock);
        assertEquals(null, bAcc);
    }

    @Test
    public void testDepositBusinessRules() throws Exception {

        Props prop = (Props) webApplicationContext.getBean("props");
        BankAccountServiceImpl bankAccountService = (BankAccountServiceImpl) webApplicationContext
                .getBean("bankAccountService");
        JSONResponse jsonResponse = bankAccountService
                .getAccountInformations(UUID.randomUUID().toString(), prop);

        JSONResponse response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgNotFound());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.deposit(UUID.randomUUID().toString(),
                prop.getSetupDepositAmount(), prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgNotFound());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.deposit(accountNo, 0, prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgDepositNegativeAmount());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.deposit(accountNo,
                (prop.getMaxDepositPerTrx() + prop.getSetupDepositAmount()),
                prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgDepositExceededMaxPerTrx());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.deposit(accountNo,
                (prop.getMaxDepositPerDay() + prop.getSetupDepositAmount()),
                prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgDepositExceededMaxPerDay());
        assertEquals(response.toString(), jsonResponse.toString());

        boolean continueRunning = true;
        while (continueRunning) {

            jsonResponse = bankAccountService.deposit(accountNo, 1, prop);
            if (jsonResponse.getMessage()
                    .equals(prop.getMsgDepositExceededFrequency())) {
                continueRunning = false;
            }
        }

        response = new JSONResponse();
        response.setStatusCode(HttpStatus.OK.value());
        response.setSuccess(false);
        response.setMessage(prop.getMsgDepositExceededFrequency());

    }

    @Test
    public void testWithdrawalBusinessRules() throws Exception {

        Props prop = (Props) webApplicationContext.getBean("props");
        BankAccountServiceImpl bankAccountService = (BankAccountServiceImpl) webApplicationContext
                .getBean("bankAccountService");

        JSONResponse jsonResponse = bankAccountService.withdraw(
                UUID.randomUUID().toString(), prop.getSetupDepositAmount(),
                prop);

        JSONResponse response = new JSONResponse();
        response.setStatusCode(HttpStatus.OK.value());
        response.setSuccess(false);
        response.setMessage(prop.getMsgNotFound());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.withdraw(accountNo, 0, prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalNegativeAmount());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.withdraw(accountNo,
                (prop.getMaxWithdrawalPerTrx() + prop.getSetupWithdrawAmount()),
                prop);

        response = new JSONResponse();
        response.setStatusCode(HttpStatus.OK.value());
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalExceeededMaxPerTrx());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.withdraw(accountNo,
                (prop.getMaxWithdrawalPerDay() + prop.getSetupWithdrawAmount()),
                prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalExceededMaxPerDay());
        assertEquals(response.toString(), jsonResponse.toString());

        BankAccountDAO dao = bankAccountService.getBankAccountDAO();
        BankAccount account = dao.getAccountInformations(accountNo, prop);

        jsonResponse = bankAccountService.withdraw(accountNo,
                (account.getBalance() + 1), prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalExceededBalance());
        assertEquals(response.toString(), jsonResponse.toString());

        boolean continueRunning = true;
        while (continueRunning) {

            jsonResponse = bankAccountService.withdraw(accountNo, 1, prop);
            if (jsonResponse.getMessage()
                    .equals(prop.getMsgWithdrawalExceededMaxFrequency())) {
                continueRunning = false;
            }
        }

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalExceededMaxFrequency());

    }

    @Test
    public void testUtilities() throws Exception {

        double balance = 1000;
        String balanceMsg = "Your balance is $" + balance + ".";
        assertEquals(Utilities.GET_BALANCE_MSG(1000), balanceMsg);

        String depositMsg = "Successfully deposited $" + depositAmount
                + " to account No " + accountNo + ". "
                + "Your new balance is $" + depositAmount + ".";
        assertEquals(Utilities.GET_DEPOSIT_SUCCESS_MSG(
                accountNo, depositAmount, depositAmount), depositMsg);

        String withdrawMsg = "Successfully withdrawn $" + withrdrawAmount
                + " from account No " + accountNo + ". Your new balance is $"
                + withrdrawAmount + ".";
        assertEquals(Utilities.GET_WITHDRAW_SUCCESS_MSG(
                accountNo, withrdrawAmount, withrdrawAmount), withdrawMsg);

    }

    @Test
    public void testhandleBadRequest() throws Exception {
        mockMvc.perform(get("/deposits")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());
    }

}
