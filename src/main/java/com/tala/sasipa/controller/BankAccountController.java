package com.tala.sasipa.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tala.sasipa.exceptions.ExceptionHandlerController;
import com.tala.sasipa.service.BankAccountService;
import com.tala.sasipa.utils.JSONResponse;
import com.tala.sasipa.utils.Props;

@CrossOrigin(origins = "*")
@RestController
public class BankAccountController extends ExceptionHandlerController {

	private static final Logger log = Logger
			.getLogger(BankAccountController.class);

	@Autowired
	private BankAccountService bankAccountService;

	@Autowired
	private Props props;

	@RequestMapping(value = "/balance/{accountNo}", method = RequestMethod.GET)
	public JSONResponse getBalance(
			@PathVariable(value = "accountNo") String accountNo) {

		log.info("New Balance Request :: Acount Number: " + accountNo);

		JSONResponse response = bankAccountService
				.getAccountInformations(accountNo, props);

		log.info("Successfully prcessed the request :: Response: " + response);

		return response;
	}

	@RequestMapping(value = "/deposit", method = RequestMethod.POST, headers = "Accept=application/json;charset=UTF-8")
	public JSONResponse deposit(
			@RequestParam(value = "accountNo", required = false, defaultValue = "ACC001") String accountNo,
			@RequestParam("amount") Double amount,
			UriComponentsBuilder builder) {
		log.info("New Deposit Request :: Acount Number: " + accountNo
				+ " :: Amount : " + amount);

		JSONResponse response = bankAccountService.deposit(accountNo, amount,
				props);

		log.info("Successfully prcessed the request :: Response: " + response);

		return response;
	}

	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	public JSONResponse withdraw(
			@RequestParam(value = "accountNo", required = false, defaultValue = "ACC001") String accountNo,
			@RequestParam(value = "amount", required = true) Double amount,
			UriComponentsBuilder builder) {
		log.info("New Withdraw Request :: Acount Number: " + accountNo
				+ " :: Amount : " + amount);

		JSONResponse response = bankAccountService.withdraw(accountNo, amount,
				props);

		log.info("Successfully prcessed the request :: Response: "
				+ response.toString());

		return response;
	}

	/**
	 * Get application properties class instance.
	 * 
	 * @return props.
	 */
	public Props getProps() {
		return props;
	}

	/**
	 * Set application properties class instance
	 * 
	 * @param props
	 */
	public void setProps(Props props) {
		this.props = props;
	}

	/**
	 * Get the bank account service instance.
	 * 
	 * @return bankAccountService
	 */
	public BankAccountService getBankAccountService() {
		return bankAccountService;
	}

	/**
	 * Set the bank account service class
	 * 
	 * @param bankAccountService
	 */
	public void setBankAccountService(BankAccountService bankAccountService) {
		this.bankAccountService = bankAccountService;
	}

}
