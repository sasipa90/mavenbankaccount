package com.tala.sasipa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.tala.sasipa.dao.BankAccountDAO;
import com.tala.sasipa.model.BankAccount;
import com.tala.sasipa.service.BankAccountService;
import com.tala.sasipa.utils.JSONResponse;
import com.tala.sasipa.utils.Props;
import com.tala.sasipa.utils.Utilities;

/**
 * Bank account transactions service class.
 * 
 * BankAccountServiceImpl.java
 * 
 * @author Parfait Pascal Sibomana
 * @date Dec 3, 2016
 */
public class BankAccountServiceImpl implements BankAccountService {

	/**
	 * Bank Account Data Access Object Instance.
	 */
	@Autowired
	private BankAccountDAO bankAccountDAO;

	@Override
	public JSONResponse getAccountInformations(String accountNumber,
			Props props) {

		JSONResponse response = new JSONResponse();
		BankAccount bankAccount = bankAccountDAO
				.getAccountInformations(accountNumber, props);

		if (null == bankAccount) {
			response.setSuccess(false);
			response.setMessage(props.getMsgNotFound());
		} else {
			response.setMessage(
					Utilities.GET_BALANCE_MSG(bankAccount.getBalance()));
		}

		return response;
	}

	@Override
	public JSONResponse deposit(String accountNumber, double amount,
			Props props) {

		JSONResponse response = new JSONResponse();

		BankAccount bankAccount = bankAccountDAO
				.getAccountInformations(accountNumber, props);
		if (null == bankAccount) {
			response.setSuccess(false);
			response.setMessage(props.getMsgNotFound());
			return response;
		}

		// Get daily deposit amount after transaction.
		double actualDailyDeposit = bankAccount.getDailyDepositAmount()
				+ amount;

		// Get daily number of deposit after transaction.
		int actualNbrOfTrx = bankAccount.getDailyDeposit() + 1;

		if (amount <= 0) {
			response.setSuccess(false);
			response.setMessage(props.getMsgDepositNegativeAmount());
			return response;
		} else if (actualDailyDeposit > props.getMaxDepositPerDay()) {
			response.setSuccess(false);
			response.setMessage(props.getMsgDepositExceededMaxPerDay());
			return response;
		} else if (amount > props.getMaxDepositPerTrx()) {
			response.setSuccess(false);
			response.setMessage(props.getMsgDepositExceededMaxPerTrx());
			return response;
		} else if (actualNbrOfTrx > props.getMaxDepositDailyFrequency()) {
			response.setSuccess(false);
			response.setMessage(props.getMsgDepositExceededFrequency());
			return response;
		} else {

			bankAccount = bankAccountDAO.deposit(bankAccount, amount, props);
			response.setSuccess(true);
			response.setMessage(Utilities.GET_DEPOSIT_SUCCESS_MSG(accountNumber,
					amount, bankAccount.getBalance()));
			return response;
		}
	}

	@Override
	public JSONResponse withdraw(String accountNumber, double amount,
			Props props) {

		JSONResponse response = new JSONResponse();

		BankAccount bankAccount = bankAccountDAO
				.getAccountInformations(accountNumber, props);
		if (null == bankAccount) {
			response.setSuccess(false);
			response.setMessage(props.getMsgNotFound());
			return response;
		}

		// Get daily withdrawn amount after the current transaction.
		double actualDailyWithdrawal = bankAccount.getDailyWithdrawalAmount()
				+ amount;

		// Get number of daily withdraw transactions.
		int actualNbrOfTrx = bankAccount.getDailyWithdrawal() + 1;

		// Get balance after the current transaction.
		double balanceAfterTrx = bankAccount.getBalance() - amount;

		if (amount <= 0) {
			response.setSuccess(false);
			response.setMessage(props.getMsgWithdrawalNegativeAmount());
			return response;
		} else if (actualDailyWithdrawal > props.getMaxWithdrawalPerDay()) {
			response.setSuccess(false);
			response.setMessage(props.getMsgWithdrawalExceededMaxPerDay());
			return response;
		} else if (amount > props.getMaxWithdrawalPerTrx()) {
			response.setSuccess(false);
			response.setMessage(props.getMsgWithdrawalExceeededMaxPerTrx());
			return response;
		} else if (actualNbrOfTrx > props.getMaxWithdrawalDailyFrequency()) {
			response.setSuccess(false);
			response.setMessage(props.getMsgWithdrawalExceededMaxFrequency());
			return response;
		} else if (balanceAfterTrx < 0) {
			response.setSuccess(false);
			response.setMessage(props.getMsgWithdrawalExceededBalance());
			return response;
		} else {

			bankAccount = bankAccountDAO.withdraw(bankAccount, amount, props);
			response.setSuccess(true);
			response.setMessage(Utilities.GET_WITHDRAW_SUCCESS_MSG(
					accountNumber, amount, bankAccount.getBalance()));
			return response;

		}

	}

	/**
	 * Get Bank Account DAO instance
	 * 
	 * @return bankAccountDAO
	 */
	public BankAccountDAO getBankAccountDAO() {
		return bankAccountDAO;
	}

	/**
	 * Set Bank Account DAO instance
	 * 
	 * @param bankAccountDAO
	 */
	public void setBankAccountDAO(BankAccountDAO bankAccountDAO) {
		this.bankAccountDAO = bankAccountDAO;
	}

}
