package com.tala.sasipa.service;


import com.tala.sasipa.utils.JSONResponse;
import com.tala.sasipa.utils.Props;

/**
 * Interface used handle bank account different services.
 * 
 * BankAccountService.java
 * 
 * @author Parfait Pascal Sibomana
 * @date Dec 3, 2016
 */
public interface BankAccountService {

	/**
	 * Get account informations
	 * @param accountNumber Bank Account Number
	 * @param props Application property instance.
	 * @return BankAccount instance
	 */
	public JSONResponse getAccountInformations(String accountNumber, Props props);

	/**
	 * Deposit amount on a specific account
	 * @param accountNumber Bank Account Number
	 * @param amount Amount to be deposited
	 * @param props Application property instance.
	 * @return BankAccount instance with updated informations
	 */
	public JSONResponse deposit(String accountNumber, double amount, Props props);

	/**
	 * Withdraw amount on a specific account
	 * @param accountNumber Bank Account Number
	 * @param amount Amount to be deposited
	 * @param props Application property instance.
	 * @return BankAccount instance with updated informations
	 */
	public JSONResponse withdraw(String accountNumber, double amount, Props props);

}
