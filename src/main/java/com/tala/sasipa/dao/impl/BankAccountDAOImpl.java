package com.tala.sasipa.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.tala.sasipa.dao.BankAccountDAO;
import com.tala.sasipa.model.BankAccount;
import com.tala.sasipa.utils.Props;

/**
 * 
 * Bank Account Data Access Object Implementation
 * 
 * BankAccountDAOImpl.java
 * 
 * @author Parfait Pascal Sibomana
 * @date Dec 4, 2016
 */
public class BankAccountDAOImpl implements BankAccountDAO {

	private static final Logger log = Logger
			.getLogger(BankAccountDAOImpl.class);

	// JDBC Template instance
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public BankAccount getAccountInformations(String accountNumber,
			Props props) {

		log.info("About to fetch bank account informations :: Account Number : "
				+ accountNumber);

		BankAccount account;

		try {

			// Fetch Balance.
			account = fetchBalance(accountNumber, props);

			// Fetch Daily Transactions informations if account exists.
			if (null != account) {
				fetchDailyTransactions(account, props);
				log.info("Successfully fetched Bank Account : "
						+ account.toString());
			} else {
				log.info("Bank account No : " + accountNumber
						+ " was not found");
			}

		} catch (SQLException e) {
			account = null;
			log.error("An exception occured while processing. Exception : "
					+ e.getLocalizedMessage());
		}

		return account;
	}

	/**
	 * Fetch account informations.
	 * 
	 * @param accountNumber
	 *            Account Number
	 * @throws SQLException
	 *             SQL Exception
	 */
	private void fetchDailyTransactions(BankAccount account, Props props)
			throws SQLException {

		log.info("About to fetch bank account daily transactions "
				+ ":: Account Number : " + account.getAccountNumber());

		String dataSQL = "SELECT trxType, COUNT(transactionId) AS "
				+ "dailyTransactions, SUM(amount) AS dailyAmountTransactions "
				+ "FROM transactions WHERE accountNo = ? "
				+ "AND trxDate = date('now') GROUP BY trxType ORDER BY trxType;";

		Connection conn = jdbcTemplate.getDataSource().getConnection();
		PreparedStatement stmt = conn.prepareStatement(dataSQL);
		stmt.setString(1, account.getAccountNumber());

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			String type = rs.getString("trxType");

			if (type.equals(props.getDeposit())) {
				account.setDailyDeposit(rs.getInt("dailyTransactions"));
				account.setDailyDepositAmount(
						rs.getDouble("dailyAmountTransactions"));
			} else if (type.equals(props.getWithdraw())) {
				account.setDailyWithdrawal(rs.getInt("dailyTransactions"));
				account.setDailyWithdrawalAmount(
						rs.getDouble("dailyAmountTransactions"));
			}
		}

		rs.close();
		stmt.close();
		conn.close();
	}

	/**
	 * Fetch account informations.
	 * 
	 * @param accountNumber
	 *            Account Number
	 * @throws SQLException
	 *             SQL Exception
	 */
	private BankAccount fetchBalance(String accountNo, Props props)
			throws SQLException {

		log.info("About to fetch bank account balance :: Account Number : "
				+ accountNo);

		BankAccount bankAccount = null;

		String selectSQL = "SELECT accountNo, balance FROM transactions "
				+ "WHERE accountNo = ? ORDER BY transactionId DESC LIMIT ?;";
		Connection conn = jdbcTemplate.getDataSource().getConnection();
		PreparedStatement stmt = conn.prepareStatement(selectSQL);
		stmt.setString(1, accountNo);
		stmt.setInt(2, props.getFetchLimit());
		ResultSet rs = stmt.executeQuery();

		log.info("Executing query : " + rs.getStatement().toString());

		if (rs.next()) {

			bankAccount = new BankAccount();
			bankAccount.setAccountNumber(rs.getString("accountNo"));
			bankAccount.setBalance(rs.getDouble("balance"));
		}

		rs.close();
		stmt.close();
		conn.close();

		return bankAccount;
	}

	@Override
	public BankAccount deposit(BankAccount account, double amount,
			Props props) {

		log.info("About to save a deposit transaction into the database."
				+ ":: Account Number : " + account.getAccountNumber() + " "
				+ ":: Amount : " + amount);

		try {

			String depositSQL = "INSERT INTO transactions (accountNo, amount, "
					+ "trxType, trxDate, balance) "
					+ "VALUES (?, ?, ?, date('now'), ?);";
			Connection conn = jdbcTemplate.getDataSource().getConnection();
			PreparedStatement stmt = conn.prepareStatement(depositSQL);
			stmt.setString(1, account.getAccountNumber());
			stmt.setDouble(2, amount);
			stmt.setString(3, props.getDeposit());
			stmt.setDouble(4, (account.getBalance() + amount));
			int result = stmt.executeUpdate();

			if (result > 0) {
				account = getAccountInformations(account.getAccountNumber(),
						props);
			}

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			account = null;
			log.error("An exception occured while processing. Exception : "
					+ e.getLocalizedMessage());
		}

		return account;
	}

	@Override
	public BankAccount withdraw(BankAccount account, double amount,
			Props props) {

		log.info("About to save a withdrawal transaction into the database."
				+ ":: Account Number : " + account.getAccountNumber() + " "
				+ ":: Amount : " + amount);

		try {

			String depositSQL = "INSERT INTO transactions (accountNo, amount, "
					+ "trxType, trxDate, balance) "
					+ "VALUES (?, ?, ?, date('now'), ?);";
			Connection conn = jdbcTemplate.getDataSource().getConnection();
			PreparedStatement stmt = conn.prepareStatement(depositSQL);
			stmt.setString(1, account.getAccountNumber());
			stmt.setDouble(2, amount);
			stmt.setString(3, props.getWithdraw());
			stmt.setDouble(4, (account.getBalance() - amount));
			int result = stmt.executeUpdate();

			if (result > 0) {
				account = getAccountInformations(account.getAccountNumber(),
						props);
			}

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			account = null;
			log.error("An exception occured while processing. Exception : "
					+ e.getLocalizedMessage());
		}

		return account;
	}

	@PostConstruct
	public void generateDatabase() {

		log.info("About to generate database schema if not existing");

		try {

			String depositSQL = "CREATE TABLE IF NOT EXISTS transactions ("
					+ "`transactionId` INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ "`accountNo` TEXT NOT NULL, "
					+ "`amount` NUMERIC NOT NULL, "
					+ "`trxType` TEXT NOT NULL, "
					+ "`trxDate` NUMERIC NOT NULL, "
					+ "`balance` NUMERIC NOT NULL" + ");";

			Connection conn = jdbcTemplate.getDataSource().getConnection();
			PreparedStatement stmt = conn.prepareStatement(depositSQL);
			stmt.executeUpdate();

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			log.error("An exception occured while generating database. "
					+ "Exception : " + e.getLocalizedMessage());
		}
	}

	@Override
	public void setupAccount(String accountNo, Props prop) {

		log.info("Setup an account : " + accountNo);

		try {

			String depositSQL = "INSERT INTO transactions (accountNo, amount, "
					+ "trxType, trxDate, balance) values (?, ?, ?, "
					+ "Date('now'), ?);";

			Connection conn = jdbcTemplate.getDataSource().getConnection();
			PreparedStatement stmt = conn.prepareStatement(depositSQL);
			stmt.setString(1, accountNo);
			stmt.setDouble(2, prop.getSetupDepositAmount());
			stmt.setString(3, prop.getDeposit());
			stmt.setDouble(4, prop.getSetupDepositAmount());
			stmt.executeUpdate();
			stmt.close();

			String withdrawSQL = "INSERT INTO transactions (accountNo, amount, "
					+ "trxType, trxDate, balance) values (?, ?, ?, Date('now'), ?);";
			stmt = conn.prepareStatement(withdrawSQL);
			stmt.setString(1, accountNo);
			stmt.setDouble(2, prop.getSetupWithdrawAmount());
			stmt.setString(3, prop.getWithdraw());
			stmt.setDouble(4, (prop.getSetupDepositAmount()
					- prop.getSetupWithdrawAmount()));
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			log.error("An exception occured while generating database. "
					+ "Exception : " + e.getLocalizedMessage());
		}
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
