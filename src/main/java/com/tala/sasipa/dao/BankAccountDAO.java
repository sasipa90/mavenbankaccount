package com.tala.sasipa.dao;

import com.tala.sasipa.model.BankAccount;
import com.tala.sasipa.utils.Props;

public interface BankAccountDAO {

	public BankAccount getAccountInformations(String accountNumber,
			Props props);

	public BankAccount deposit(BankAccount account, double amount, Props props);

	public BankAccount withdraw(BankAccount account, double amount,
			Props props);

	public void setupAccount(String account, Props prop);
}
