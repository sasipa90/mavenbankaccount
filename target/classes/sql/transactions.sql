CREATE TABLE IF NOT EXISTS transactions (
  `transactionId` INTEGER PRIMARY KEY AUTOINCREMENT,
  `accountNo` TEXT NOT NULL,
  `amount` NUMERIC NOT NULL,
  `trxType` TEXT NOT NULL,
  `trxDate` NUMERIC NOT NULL,
  `balance` NUMERIC NOT NULL
);

INSERT INTO transactions (accountNo, amount, trxType, trxDate, balance) values ('ACC001', 1000, 'deposit', Date('now'), 1000);
INSERT INTO transactions (accountNo, amount, trxType, trxDate, balance) values ('ACC001', 200, 'withdraw', Date('now'), 800);
